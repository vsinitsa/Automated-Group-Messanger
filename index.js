const express = require('express');
const exphbs = require('express-handlebars');
const twilio = require('twilio');
const cronJob = require('cron').CronJob;
const fs = require('fs');
//const async = require('async');
var TWILIO_ACCOUNT_SID = '',
    TWILIO_AUTH_TOKEN = '',
    TWILIO_NUMBER = '';

var client;

var rawNumbers = fs.readFileSync('numbers.json');
var numbers = JSON.parse(rawNumbers);

var rawConfig = fs.readFileSync('config.json');
var config = JSON.parse(rawConfig);

if(config["twilio_secret"] != "empty" && config["twilio_token"] != "empty" && config["twilio_number"] != empty){
    TWILIO_ACCOUNT_SID = config["twilio_secret"];
    TWILIO_AUTH_TOKEN = config["twilio_token"];
    TWILIO_NUMBER = config["twilio_number"];
    client = new twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
}


var messagesJob = new cronJob('*/1 * * * *', function(){
    if(messages.length == 0){
        messagesJob.stop();
        running = false;
    }else{
        var txttosnd = messages.splice(0,1);
        async.each(numbers, (number, callback) => {
            client.sms.messages.create({
                to: number,
                from:TWILIO_NUMBER,
                body:txttosnd
            }, (err, message) => {
                if (message != undefined) {

                console.log("SID:", message.sid);
                console.log("Date:", message.dateCreated);
                }

                callback(err);
            });
        }, (err) => {
            if (err) console.error(err);
            console.log("All messages have been sent");
        });
    }
}, null, false);

var messages = [];

var hbs = exphbs.create();
var running = false;
var loggedIn = false;

var bodyparser = require('body-parser');
var app = express();
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));
app.use(express.static('Resources'));

app.get('/', (req, res) =>{
    res.render('home', {message: messages, number:numbers, started: running});
});

app.get('/login', (req, res) =>{
    res.render('login', {secret: TWILIO_ACCOUNT_SID, token: TWILIO_AUTH_TOKEN, number: TWILIO_NUMBER});
});

app.post('/addmsg', (req, res)=>{
    var msg = req.body.message;
    messages.push(msg);
    res.end("done");
});

app.post('/addnum', (req, res)=>{
    var num = req.body.number;
    var name = req.body.name;
    num = "+1" + num;
    numbers[name] = num;
    fs.writeFileSync('numbers.json', JSON.stringify(numbers, null, 2));
    //numbers.push(num)
    res.end("done");
});

app.get('/start', (req, res)=>{
 messagesJob.start();
    running = true;
});

app.post('/activate', (req, res)=>{
    TWILIO_AUTH_TOKEN = req.body.token;
    TWILIO_ACCOUNT_SID = req.body.secret;
    config["twilio_secret"] = req.body.secret;
    config["twilio_token"] = req.body.token;
    try{
        client = new twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);
        fs.writeFileSync('config.json', JSON.stringify(config, null, 2));
    }catch(err){
        console.log(err);
    }
    res.end("done");
});

app.listen(3000);
